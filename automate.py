#!/usr/bin/python3

import argparse
import inspect
import os
import sys
import subprocess
import time
import itertools
import re
import numpy as np
import random


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result


def parse_data_from_serial_output(serial_output_file, group_config: dict):
    result = {
        group_number: {channel: [] for channel in channels}
        for group_number, channels in group_config.items()
    }
    with open(serial_output_file) as file:
        for i, line in enumerate(filter(lambda line: line[0] == "#", file.readlines())):
            if "\x02" in line:
                continue
            group_number_match = re.compile(r"#(\d+):").search(line)
            if group_number_match is None or group_number_match.start() != 0:
                # raise Exception(
                #     f"Line {i} starts with # but has no recognizable group number"
                # )
                continue
            group_number = group_number_match.group(1)
            if group_number not in group_config.keys():
                continue
            channel_values = line[group_number_match.end() :].split(" | ")
            if len(channel_values) != len(group_config[group_number]):
                continue
            for i, value in enumerate(channel_values):
                try:
                    value = float(value)
                except ValueError:
                    value = None
                result[group_number][group_config[group_number][i]].append(value)
    return result


HALL_SENSOR_DELTA_S = 0.204204  # Diameter of wheel in meters


def cruise_controller_step_response(python_path: str, serial_port: str):
    setup_commands = "\n".join(
        [
            "set command_delay 0",
            "",
            "!reset now",
            "wait 2",
            "",
            "!steer 450",
            "!drv f 100",
            "wait 5",
            "",
            "!drv f -100",
            "wait 2",
        ]
    ).encode("ascii")
    # subprocess.run([python_path, r("pyserial_ucboard.py"), "-e", serial_port], input=setup_commands, stdout=sys.stdout)
    t_s = 50
    for k_p in [0, 50, 100, 200, 500, 1000, 2000]:
        for k_i in [0, 5, 10, 20, 50, 100, 200, 500]:
            for requested_speed in [500, 1000, 1500]:
                output_file = r(
                    f"automated_outputs/cruise_controller_step_response/serial_output_ts{t_s}_ff+p{k_p}+i{k_i}_{requested_speed}mmps.txt"
                )
                if os.path.exists(output_file):
                    continue
                input_commands = "\n".join(
                    [
                        "set command_delay 0",
                        "",
                        f"!globalvar g_carbasicfcts_cruiseTS {t_s}",
                        f"!globalvar g_carbasicfcts_cruiseKP {k_p}",
                        f"!globalvar g_carbasicfcts_cruiseKI {k_i}",
                        "wait 1",
                        "",
                        "!daq grp 4 ~all _tics cruise_drvval hall_cnt hall_dt hall_dt8",
                        "!daq start",
                        "",
                        f"!drv c {requested_speed}",
                        "wait 10",
                        "!daq stop",
                        "",
                        "!drv f -100",
                        "wait 2",
                        "",
                    ]
                ).encode("ascii")
                # print(input_commands.decode("ascii"))
                subprocess.run(
                    [
                        python_path,
                        r("pyserial_ucboard.py"),
                        "--output-file",
                        output_file,
                        "-ce",
                        serial_port,
                    ],
                    input=input_commands,
                    stdout=sys.stdout,
                )


def find_feed_forward_values(python_path: str, serial_port: str):
    setup_commands = "\n".join(
        [
            "set command_delay 0",
            "",
            "!reset now",
            "wait 2",
            "",
            "!steer -800",
            "!drv f 300",
            "wait 5",
            "",
            "!drv f -100",
            "wait 2",
        ]
    ).encode("ascii")
    # subprocess.run([python_path, r("pyserial_ucboard.py"), "-e", serial_port], input=setup_commands, stdout=sys.stdout)
    results = {
        # "f 50": None,
        # "f 100": None,
        "f 200": None,
        "f 300": None,
        "f 400": None,
        "f 500": None,
        "f 600": None,
        "f 700": None,
        "f 800": None,
        "f 900": None,
        "f 1000": None,
        # "b 50": None,
        # "b 100": None,
        "b 200": None,
        "b 300": None,
        "b 400": None,
        "b 500": None,
    }
    for drvcommand in results.keys():
        output_file = r(
            f"automated_outputs/find_feed_forward_values/serial_output_drv_{drvcommand}.txt"
        )
        # if os.path.exists(output_file):
        #     continue
        input_commands = "\n".join(
            [
                "set command_delay 0",
                "",
                "!daq grp 4 ~all _tics hall_cnt hall_dt hall_dt8",
                "!daq start",
                "",
                f"!drv {drvcommand}",
                "wait 15",
                "!daq stop",
                "",
                "!drv f -100",
                "wait 2",
                "",
            ]
        ).encode("ascii")
        # print(input_commands.decode("ascii"))
        # subprocess.run([python_path, r("pyserial_ucboard.py"), "--output-file", output_file, "-ce", serial_port], input=input_commands, stdout=sys.stdout)
        data = parse_data_from_serial_output(
            output_file,
            {
                "4": ["_tics", "hall_cnt", "hall_dt", "hall_dt8"],
            },
        )
        speed = HALL_SENSOR_DELTA_S / (np.array(data["4"]["hall_dt8"]) * 1e-3)
        # print(speed)
        average_speed = np.average(speed[-len(speed) // 3 :])
        results[drvcommand] = average_speed
    results["f 0"] = 0
    print(results)

    results_csv_file = r("automated_outputs/find_feed_forward_values/results.csv")
    results_config_file = r("automated_outputs/find_feed_forward_values/results.h")
    # os.makedirs(os.path.dirname(results_csv_file), exist_ok=True)
    result_numbers = [
        (
            int(key[2:]) * (1 if key[0] == "f" else -1),
            results[key] * (1 if key[0] == "f" else -1),
        )
        for key in results
    ]
    result_numbers = sorted(result_numbers, key=lambda pair: pair[0])

    with open(results_csv_file, "w") as file:
        file.write(
            "\n".join([",".join([str(col) for col in cols]) for cols in result_numbers])
        )
    with open(results_config_file, "w") as file:
        pairs = [f"{{{1000*speed:.0f},{drvval}}}" for drvval, speed in result_numbers]
        content = (
            "#define CONFIG_CRUISE_CONTROL_FF_REFERENCE {"
            + ",".join(pairs)
            + "}\n#define CONFIG_CRUISE_CONTROL_FF_REFERENCE_LENGTH "
            + str(len(pairs))
            + "\n"
        )
        file.write(content)


def dos_attack(python_path: str, serial_port: str):
    setup_commands = "\n".join(
        [
            "set command_delay 0",
            "",
            "!reset now",
            "wait 2",
            "",
            # "!steer 100",
            # "!drv f 100",
            # "wait 5",
            # "",
            # "!drv f -100",
            # "wait 2",
        ]
    ).encode("ascii")
    subprocess.run(
        [python_path, r("pyserial_ucboard.py"), "-e", serial_port],
        input=setup_commands,
        stdout=sys.stdout,
    )
    input_commands = "\n".join(
        [
            "set command_delay 0",
            "",
            "!us on",
            "",
            # *[
            #     f"!daq grp {group_number} ~any _tics _dly ax gy mz usf usl usr usb vdbat"
            #     for group_number in range(20)
            # ],
            # "!daq start",
            # "!drv c 1000",
            *itertools.chain(
                *[
                    [
                        "!led a on",
                        "!led a off",
                    ]
                    for _ in range(3)
                ]
            ),
            "",
            "!drv off",
            "",
        ]
    ).encode("ascii")
    output_file = r("automated_outputs/dos_attack/serial_output.txt")
    # print(input_commands.decode("ascii"))
    subprocess.run(
        [
            python_path,
            r("pyserial_ucboard.py"),
            "--output-file",
            output_file,
            "-cer",
            serial_port,
        ],
        input=input_commands,
        stdout=sys.stdout,
    )


def drv_response_time(python_path: str, serial_port: str):
    for _ in range(50):
        setup_commands = "\n".join(
            [
                "set command_delay 0",
                "",
                "!reset now",
                "wait 2",
                "",
                # "!steer 100",
                # "!drv f 100",
                # "wait 5",
                # "",
                # "!drv f -100",
                # "wait 2",
            ]
        ).encode("ascii")
        subprocess.run(
            [python_path, r("pyserial_ucboard.py"), "-e", serial_port],
            input=setup_commands,
            stdout=sys.stdout,
        )
        delay = random.uniform(0.1, 1)
        # delay = 0
        input_commands = "\n".join(
            [
                "set command_delay 0",
                "",
                "!drv f 300",
                "wait 2",
                "",
                "!led a on",
                f"wait {delay:.3f}",
                "",
                "# Measure response time",
                "!drv f -300",
                "",
                "!led a off",
                "",
            ]
        ).encode("ascii")
        output_file = r("automated_outputs/drv_response_time/serial_output.txt")
        results_file = r("automated_outputs/drv_response_time/results.csv")
        # print(input_commands.decode("ascii"))
        subprocess.run(
            [
                python_path,
                r("pyserial_ucboard.py"),
                "--output-file",
                output_file,
                "-cer",
                serial_port,
            ],
            input=input_commands,
            stdout=sys.stdout,
        )
        with open(output_file) as serial_output:
            for line in serial_output.readlines():
                response_time_match = re.compile(r":F -300 \((\d+\.\d+) ms\)").search(
                    line
                )
                if response_time_match is None:
                    continue
                response_time = float(response_time_match.group(1))
                os.makedirs(os.path.dirname(results_file), exist_ok=True)
                with open(results_file, "a") as results:
                    delay_ms = delay * 1000
                    results.write(f"{delay_ms:.2f},{response_time:.2f}\n")
                break


def drv_response_time_static(python_path: str, serial_port: str):
    for _ in range(50):
        delay = random.uniform(0.1, 1)
        # delay = 0
        input_commands = "\n".join(
            [
                "set command_delay 0.1",
                "",
                "!reset now",
                "wait 2",
                "",
                "!drv f 300",
                "wait 2",
                "",
                "set command_delay 0",
                "!led a on",
                f"wait {delay:.3f}",
                "set command_delay 0.1",
                "",
                "# Measure response time",
                "!drv f -300",
                "",
                "!led a off",
                "",
            ]
        ).encode("ascii")
        output_file = r("automated_outputs/drv_response_time/serial_output.txt")
        results_file = r("automated_outputs/drv_response_time/results.csv")
        # print(input_commands.decode("ascii"))
        subprocess.run(
            [
                python_path,
                r("pyserial_ucboard.py"),
                "--output-file",
                output_file,
                "-cer",
                "-t",
                "0",
                serial_port,
            ],
            input=input_commands,
            stdout=sys.stdout,
        )
        with open(output_file) as serial_output:
            for line in serial_output.readlines():
                response_time_match = re.compile(r":F -300 \((\d+\.\d+) ms\)").search(
                    line
                )
                if response_time_match is None:
                    continue
                response_time = float(response_time_match.group(1))
                os.makedirs(os.path.dirname(results_file), exist_ok=True)
                with open(results_file, "a") as results:
                    delay_ms = delay * 1000
                    results.write(f"{delay_ms:.2f},{response_time:.2f}\n")
                break


def main(function_name: str, python_path: str, serial_port: str):
    try:
        globals().get(function_name)(python_path, serial_port)
    except KeyboardInterrupt:
        subprocess.run(
            [python_path, r("pyserial_ucboard.py"), "-e", serial_port],
            input="!led a off".encode("ascii"),
            stdout=sys.stdout,
        )
        subprocess.run(
            [python_path, r("pyserial_ucboard.py"), "-e", serial_port],
            input="!drv off".encode("ascii"),
            stdout=sys.stdout,
        )


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    for parameter in inspect.signature(main).parameters:
        argument_parser.add_argument(parameter)
    arguments = argument_parser.parse_args()
    main(**vars(arguments))
