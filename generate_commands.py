#!/usr/sbin/python3

# usage: generate_commands.py [-h] [--output-file OUTPUT_FILE]

# This scripts generates command input for `pyserial_ucboard.py`. Edit this script
# directly to change which commands it generates. Commands could either be stored to a
# file which is then used as input file for `pyserial_ucboard.py` like so: `python
# generate_commands.py -o commands.txt && python pyserial_ucboard.py -i commands.txt -e
# /dev/ttyUSBX`, or you can pipe the commands directly like so: `python
# generate_commands.py | python pyserial_ucboard.py -e /dev/ttyUSBX`. `

# options:
#   -h, --help            show this help message and exit
#   --output-file OUTPUT_FILE, -o OUTPUT_FILE
#                         Path to file for storing output

import sys
import time
import threading
import argparse
import os
import time
import itertools


def r(*path):
    """
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)

def run_in_dir(directory, callable):
    cwd = os.getcwd()
    os.chdir(directory)
    result = callable()
    os.chdir(cwd)
    return result

commands = [
    "set command_delay 0",
    "",
    "!reset now",
    "wait 2",
    "",
    "!steer 100",
    "wait 1",
    "",
    *itertools.chain(
        *[[
            f"# Driving 10s with drvval {drvval}",
            f"!drv f {drvval}",
            "wait 10",
            "!drv f -100",
            "wait 2",
            "",
        ] for drvval in range(100, 1001, 100)]
    ),
]

# print(commands); exit()

commands_txt = "\n".join(commands)

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser(
        description="""\
This scripts generates command input for `pyserial_ucboard.py`.
Edit this script directly to change which commands it generates.
Commands could either be stored to a file which is then used as input file for `pyserial_ucboard.py` like so:
`python generate_commands.py -o commands.txt && python pyserial_ucboard.py -i commands.txt -e /dev/ttyUSBX`,
or you can pipe the commands directly like so:
`python generate_commands.py | python pyserial_ucboard.py -e /dev/ttyUSBX`.
`""",
    )
    argument_parser.add_argument("--output-file", "-o", type=str, default=None, help="Path to file for storing output")
    args = argument_parser.parse_args()
    output_file = sys.stdout
    if args.output_file is not None:
        output_file = open(args.output_file, "w")
    print(commands_txt, file=output_file)
