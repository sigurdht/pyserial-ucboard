#!/usr/sbin/python3

# usage: dummy_ucboard.py [-h] [--baudrate BAUDRATE] [--delay DELAY] serial_port

# This script can be used for testing/debugging `pyserial_ucboard.py` or other command
# passing workflows. To use it, firstly set up a virutal serial port connection. On
# linux this can be done with socat: `socat -d -d pty,raw,echo=0 pty,raw,echo=0` This
# command will output two serial devices (e.g. `/dev/pts/X` and `/dev/pts/Y`) that are
# now connected. Leave this process running. Now, in another terminal, run `python
# dummy_ucboard.py /dev/pts/X` and leave also this process running. Then, run your
# command passing workflow with `/dev/pts/Y` as serial port. The dummy is, as the name
# suggests, quite stupid and can only print the commands it receives and respond with
# `:ok`.

# positional arguments:
#   serial_port           Serial communication port to use

# options:
#   -h, --help            show this help message and exit
#   --baudrate BAUDRATE, -b BAUDRATE
#                         Baudrate for serial communication (default: 921600)
#   --delay DELAY, -d DELAY
#                         Delay in seconds before responding to commands (default:
#                         0.001)

import serial
import serial.tools.list_ports
import sys
import time
import threading
import argparse
import os
import time

def r(*path):
    """"
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


if __name__ == "__main__":
    class ArgumentParserCustom(argparse.ArgumentParser):
        def error(self, message):
            sys.stderr.write(f"error: {message}\n")
            self.print_help()
            sys.exit(2)

    argument_parser = ArgumentParserCustom(
        epilog="Available serial ports are: "+", ".join([str(port) for port in  serial.tools.list_ports.comports()]),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""\
This script can be used for testing/debugging `pyserial_ucboard.py` or other command passing workflows.
To use it, firstly set up a virutal serial port connection. On linux this can be done with socat:
`socat -d -d pty,raw,echo=0 pty,raw,echo=0`
This command will output two serial devices (e.g. `/dev/pts/X` and `/dev/pts/Y`) that are now connected. Leave this process running.
Now, in another terminal, run `python dummy_ucboard.py /dev/pts/X` and leave also this process running.
Then, run your command passing workflow with `/dev/pts/Y` as serial port.
The dummy is, as the name suggests, quite stupid and can only print the commands it receives and respond with `:ok`."""
    )
    argument_parser.add_argument("--baudrate", "-b", type=int, default=921600, help="Baudrate for serial communication")
    argument_parser.add_argument("--delay", "-d", type=float, default=.001, help="Delay in seconds before responding to commands")
    argument_parser.add_argument("serial_port", type=str, help="Serial communication port to use")
    args = argument_parser.parse_args()

    serial_port = args.serial_port

    try:
        serial_connection = serial.Serial(port=serial_port, baudrate=args.baudrate, timeout=.1)
    except Exception as exception:
        print(exception)
        print("Something went wrong. Did you enter the correct serial port?")
        print("Available ports are:")
        for port in serial.tools.list_ports.comports():
            print(f"    {port}")
        print("If you are on Linux you might have to get permission to read/write to the device with")
        print(f"    sudo chmod a+rw {serial_port}")
        sys.exit(1)

    print(f"Successfully connected to commander on {serial_port}")
    print("Press Ctrl+C to exit")

    try:
        while True:
            command = serial_connection.read_until(expected=b"\n")
            if command == b"":
                continue
            command = command.decode(encoding="ascii")
            print(command, end="")
            time.sleep(args.delay)
            serial_connection.write(b"\x02:ok\x03")
    except (KeyboardInterrupt):
        # Exit on Ctrl+C
        pass
