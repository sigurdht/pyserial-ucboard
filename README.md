# UCBoard-Tools

This repository is a collection of tools for developing the software for the UCBoard made by Sigurd Tullander during his master thesis in the summer semestre 2023. They will for the most part serve best as examples or starting points for finding an efficient workflow. Below comes a quick description of what the different tools do.

The tools require the `numpy`, `pyserial` and `prompt-toolkit` packages:

```
pip install numpy pyserial prompt-toolkit
```

## `pyserial_ucboard.py`

`pyserial_ucboard.py` is a script that lets you send commands to the UCBoard and receive responses via a command line interface. For interactive use, the script can be launched with

```
python pyserial_ucboard.py <serial_port>
```

when the UCBoard is attached to `<serial_port>`. The value for `<serial_port>` is typically `/dev/ttyUSB0` on Linux and `COM7` on Windows. A list of available serial ports as well as more command line options are printed when running

```
python pyserial_ucboard.py --help
```

## `automate.py`

A collection of automation scripts to do speed and response time measurements with the UCBoard using `pyserial_ucboard.py`.

## `dummy_ucboard.py`

This script can be used for testing/debugging `pyserial_ucboard.py` or other command
passing workflows. To use it, firstly set up a virutal serial port connection. On
linux this can be done with socat: `socat -d -d pty,raw,echo=0 pty,raw,echo=0` This
command will output two serial devices (e.g. `/dev/pts/X` and `/dev/pts/Y`) that are
now connected. Leave this process running. Now, in another terminal, run `python
dummy_ucboard.py /dev/pts/X` and leave also this process running. Then, run your
command passing workflow with `/dev/pts/Y` as serial port. The dummy is, as the name
suggests, quite stupid and can only print the commands it receives and respond with
`:ok`.

## `generate_commands.py`

This scripts generates command input for `pyserial_ucboard.py`. Edit this script
directly to change which commands it generates. Commands could either be stored to a
file which is then used as input file for `pyserial_ucboard.py` like so: `python
generate_commands.py -o commands.txt && python pyserial_ucboard.py -i commands.txt -e
/dev/ttyUSBX`, or you can pipe the commands directly like so: `python
generate_commands.py | python pyserial_ucboard.py -e /dev/ttyUSBX`. `
