#!/usr/sbin/python3

# usage: pyserial_ucboard.py [-h] [--baudrate BAUDRATE] [--timeout TIMEOUT]
#                            [--history-file HISTORY_FILE] [--input-file INPUT_FILE]
#                            [--exit-after-input-commands] [--output-file OUTPUT_FILE]
#                            [--clear-output-file] [--response-time]
#                            serial_port [command ...]

# positional arguments:
#   serial_port           Serial communication port to use
#   command               Send command, listen for response and exit (default: None)

# options:
#   -h, --help            show this help message and exit
#   --baudrate BAUDRATE, -b BAUDRATE
#                         Baudrate for serial communication (default: 921600)
#   --timeout TIMEOUT, -t TIMEOUT
#                         Timeout in seconds when waiting for response (default: 3)
#   --history-file HISTORY_FILE, -f HISTORY_FILE
#                         Path to file for reading and storing history of previously
#                         sent commands (default: None)
#   --input-file INPUT_FILE, -i INPUT_FILE
#                         Path to file for input commands. Will be overridden by
#                         command provided directly through positional argument
#                         (default: None)
#   --exit-after-input-commands, -e
#                         Exit after provided input commands are executed (default:
#                         False)
#   --output-file OUTPUT_FILE, -o OUTPUT_FILE
#                         Path to file for storing output (default: None)
#   --clear-output-file, -c
#                         Clear output file on startup (default: False)
#   --response-time, -r   Prints how long time it takes from a command is sent until
#                         the direct response is received (default: False)

import serial
import serial.tools.list_ports
import sys
import time
import threading
import argparse
import os
import time

from prompt_toolkit import PromptSession
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.key_binding.bindings.named_commands import end_of_line
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.history import FileHistory
from prompt_toolkit.patch_stdout import patch_stdout

def r(*path):
    """"
    Takes a relative path from the directory of this python file and returns the absolute path.
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)


def read_message_from_ucboard(serial_connection: serial.Serial):
    # output = serial_connection.read()
    # return None if output == b"" else output.decode(encoding="ascii")
    # Read until start of message
    output = serial_connection.read_until(expected=b"\x02")

    if output == b"":
        return None

    # Read until end of message, do not include the \x03 in result, strip eventual extra leading \x02
    # and decode (convert from bytes to str)
    return (serial_connection.read_until(expected=b"\x03")[:-1]).strip(b"\x02").decode(encoding="ascii")


if __name__ == "__main__":
    class ArgumentParserCustom(argparse.ArgumentParser):
        def error(self, message):
            sys.stderr.write(f"error: {message}\n")
            self.print_help()
            sys.exit(2)

    argument_parser = ArgumentParserCustom(
        epilog="Available serial ports are: "+", ".join([str(port) for port in  serial.tools.list_ports.comports()]),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    argument_parser.add_argument("--baudrate", "-b", type=int, default=921600, help="Baudrate for serial communication")
    argument_parser.add_argument("--timeout", "-t", type=float, default=3, help="Timeout in seconds when waiting for response")
    argument_parser.add_argument("--history-file", "-f", type=str, default=None, help="Path to file for reading and storing history of previously sent commands")
    argument_parser.add_argument("--input-file", "-i", type=str, default=None, help="Path to file for input commands. Will be overridden by command provided directly through positional argument")
    argument_parser.add_argument("--exit-after-input-commands", "-e", action="store_true", help="Exit after provided input commands are executed")
    argument_parser.add_argument("--output-file", "-o", type=str, default=None, help="Path to file for storing output")
    argument_parser.add_argument("--clear-output-file", "-c", action="store_true", help="Clear output file on startup")
    argument_parser.add_argument("--response-time", "-r", action="store_true", help="Prints how long time it takes from a command is sent until the direct response is received")
    argument_parser.add_argument("serial_port", type=str, help="Serial communication port to use")
    argument_parser.add_argument("command", type=str, nargs="*", help="Send command, listen for response and exit")
    args = argument_parser.parse_args()

    serial_port = args.serial_port

    try:
        serial_connection = serial.Serial(port=serial_port, baudrate=args.baudrate, timeout=.1)
    except Exception as exception:
        print(exception)
        print("Something went wrong. Did you enter the correct serial port?")
        print("Available ports are:")
        for port in serial.tools.list_ports.comports():
            print(f"    {port}")
        print("If you are on Linux you might have to get permission to read/write to the device with")
        print(f"    sudo chmod a+rw {serial_port}")
        sys.exit(1)

    time.sleep(0.1)

    if args.output_file is not None:
        os.makedirs(os.path.dirname(args.output_file), exist_ok=True)
        if args.clear_output_file:
            with open(args.output_file, "w") as output_file:
                output_file.write("")

    exit_event = threading.Event()
    response_received_event = threading.Event()

    output_lock = threading.Lock()

    last_command_time = time.time()

    def print_to_output_file(output, *print_args, **print_kwargs):
        if args.output_file is not None:
            with open(args.output_file, "a") as output_file:
                print(output, *print_args, file=output_file, **print_kwargs)

    def read_output_thread():
        global serial_connection, exit_event, last_command_time

        while not exit_event.is_set():
            message = read_message_from_ucboard(serial_connection)

            if message is None or message == "":
                continue

            output_lock.acquire()
            if message[0] == ":":
                response_received_event.set()
                if args.response_time:
                    time_since_last_command_ms = (time.time() - last_command_time) * 1000
                    message = f"{message} ({time_since_last_command_ms:.2f} ms)"
            print(message)
            print_to_output_file(message)
            output_lock.release()
    
    def send_command(command):
        global serial_connection, last_command_time
        response_received_event.clear()
        serial_connection.write((command + "\n").encode(encoding="ascii"))
        last_command_time = time.time()
    
    if len(args.command) > 0:
        command = " ".join(args.command)
        command = (command + "\n").encode(encoding="ascii")
        serial_connection.write(command)
        print_to_output_file(f"> {command}")
        while True:
            answer = read_message_from_ucboard(serial_connection)
            if answer is None:
                break
            print(answer)
            print_to_output_file(answer)
        if args.exit_after_input_commands:
            sys.exit()

    threading.Thread(target=read_output_thread).start()

    print(f"Successfully connected to ucboard on {serial_port}")
    print("Type the command you want to send below and press ENTER to send it")
    print("Press Ctrl+C to exit")

    try:
        input_commands = None
        if args.input_file is not None:
            with open(args.input_file) as input_file:
                input_commands = input_file.read().split("\n")
        elif not sys.stdin.isatty():
            input_commands = sys.stdin.read().split("\n")

        if input_commands is not None:
            command_delay = 0.1
            for command in input_commands:
                if command == "" or command[0] == "#":
                    # Ignore empty lines and comments
                    continue
                if command[0] in ["!", "?"]:
                    # Command to ucboard
                    output_lock.acquire()
                    send_command(command)
                    print(f"> {command}")
                    print_to_output_file(f"> {command}")
                    output_lock.release()
                    time.sleep(command_delay)
                    response_received_event.wait(timeout=args.timeout)
                    continue
                if command.startswith("wait"):
                    # Wait for the specified number of seconds
                    time.sleep(float(command[len("wait "):]))
                if command.startswith("set command_delay"):
                    command_delay = float(command[len("set command_delay "):])
            if args.exit_after_input_commands:
                raise KeyboardInterrupt()
        prompt_session = PromptSession(
            history=FileHistory(args.history_file or r("history.pthistory")),
            auto_suggest=AutoSuggestFromHistory(),
        )
        while True:
            with patch_stdout():
                command = prompt_session.prompt("> ")
            output_lock.acquire()
            send_command(command)
            print_to_output_file(f"> {command}")
            output_lock.release()
            response_received_event.wait(timeout=args.timeout)
    except (KeyboardInterrupt, EOFError):
        # Exit on Ctrl+C and Ctrl+D
        exit_event.set()
